import os
from PIL import Image, ImageEnhance


def reduce_opacity(im, opacity):
    """Returns an image with reduced opacity."""
    # Checking if opacity is between 0 and 1
    assert 0 <= opacity <= 1
    # If the mode of th image is not RGBA then it will be converted into RGBA
    if im.mode != 'RGBA':
        im = im.convert('RGBA')
    else:
        # If it is then we just copy the image
        im = im.copy()

    # Split the image into the four channels and take the fourth one
    alpha = im.split()[3]
    # Change the image by the given opacity
    alpha = ImageEnhance.Brightness(alpha).enhance(opacity)
    # Combine the four channels together
    im.putalpha(alpha)
    return im


def watermark(host, mark, position, opacity):
    """Adds a watermark to an image."""
    # if the given opacity is smaller than one than it means we want to reduce it
    if opacity < 1:
        mark = reduce_opacity(mark, opacity)
    if host.mode != 'RGBA':
        host = host.convert('RGBA')

    # paste/combine the watermark with the host image and the mask in a given position
    host.paste(mark, position, mask=mark)

    return host


def test(image):
    # Load/open the host/watermark image and copy it, to not use the original one
    host = Image.open(image)
    host = host.copy()
    mark = Image.open('C:/Users/Esada Licina/UNI.lu BICS/BICS sem3/BSP3/Technical/Personal watermark/wat.png')
    # Resize the watermark image, to mak eit smaller than the host image
    mark = mark.resize((50, 50))
    # Choose a position which is the bottom right of the host image
    position = ((host.width - mark.width), (host.height - mark.height))

    return watermark(host, mark, position, 1)


if __name__ == '__main__':
    # This is the path of the host images
    images = "C:/Users/Esada Licina/UNI.lu BICS/BICS sem3/BSP3/Technical/Images/"
    # The chosen name for the watermarked images
    WI_name = 0
    # Take every single host image from the directory
    for single_img in os.listdir(images):
        img = images + single_img
        # Show the results
        test(img).show()
        # Save the results in a given directory
        test(img).save('C:/Users/Esada Licina/UNI.lu BICS/BICS '
                       'sem3/BSP3/Technical/visible watermarked Images/' +
                       'watermarkedImage' + str(WI_name) + '.png')
        WI_name += 1
